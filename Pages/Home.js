import { StatusBar } from 'expo-status-bar';
import {StyleSheet, Text, View, FlatList } from 'react-native';
import React, { useState, useEffect } from 'react';
import PokeApi, {getPokemons} from "../Utils/API/PokeApi"
import TitlePokemon from "../Components/Pokemon";
import Navigation from "../Navigation/Navigation";
import 'react-native-gesture-handler';

export default function Home(props) {

    const { navigation, route, ...restProps} = props

    const [textParent, setTextParent] = useState("Default")
    const [listPokemon, setListPokemon] = useState([])
    const [nextPage, setNextPage] = useState("https://pokeapi.co/api/v2/pokemon")


    useEffect(() => {
        loadPokemon(nextPage)
    }, []);

    const loadPokemon = (url) => {
        getPokemons(url).then(datas => {
            setListPokemon([...listPokemon, ...datas.results])
            setNextPage(datas.next)
        });
    }

    return (
        <View style={styles.container}>
            <FlatList data={listPokemon}
                      numColumns={3}
                      renderItem={({item}) => <TitlePokemon name={item.name} url={item.url} navigation={navigation} route={route} />}
                      //navigation={navigation}
                      keyExtractor={(item) => item.name}
                      onEndReachedThreshold={0.5}
                      onEndReached={() => {
                          loadPokemon(nextPage)
                      }}/>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: "#f6cece",
        flex:1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#0033ff',
        borderRadius:50,
        color: "#9c1e1e",

        // padding: 20,
        marginVertical: 5,

    },
    title: {
        fontSize: 22,
        color: '#000000',
    },
});