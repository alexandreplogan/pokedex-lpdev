import React, { useState, useEffect } from 'react';
import {Image, StyleSheet, Text, View, ScrollView} from "react-native";
import {getPokemons} from "../Utils/API/PokeApi"
import baseImage from "../assets/pokeball.png";

export function getMoveType(url) {

    return fetch(url.type.name)
        .then((response) => response.json())
        .catch((error) => console.log('error', error));
}
export default function PokemonDetail(props) {

    const { navigation, route, ...restProps } = props

    const {pokemonDatas} = route.params



    const pokemonImg = pokemonDatas.sprites.front_default

    const PokemonAbilities = pokemonDatas.abilities.map((item, index) => {


        return <Text key={index}>{item.ability.name}{"\n"}</Text>;
    });

    const PokemonTypes = pokemonDatas.types.map((item, index) => {

        // const typeWater = pokemonDatas.abilities.slot[1];
        return <Text key={index}>{item.type.name}{"\n"} </Text>;
    });
    const PokemonStat = pokemonDatas.stats.map((item, index) => {


        return<Text key={index}>  {item.stat.name} : {item.base_stat}{"\n"}</Text>;
    });
    const PokemonMoves = pokemonDatas.moves.map((item, index) => {

        // {item.url.type.name}

        //Créer un composant enfant pour les Compétences
        //{item.move.url}

        return<Text key={index}>  - {item.move.name}{"\n"}  </Text>;
    });
    return (

    <View style={styles.fiches}>
    <ScrollView>
        <Text style={styles.PokemonName}>{pokemonDatas.name}</Text>

        {
            pokemonImg ?
                ( <Image style={styles.image} source={{uri : pokemonImg }} /> ):
                ( <Image style={styles.image} source={ baseImage } /> )
        }
        <Text>Type:</Text>
        <Text style={styles.PokemonAbilities}>{PokemonTypes}</Text>

        <Text>Stats:</Text>
        <Text style={styles.PokemonAbilities}>{PokemonStat}</Text>

        <Text>Capacités Spéciales :</Text>
        <Text style={styles.PokemonAbilities}>{PokemonAbilities}</Text>
        <Text>Capacités :</Text>
        <Text style={styles.PokemonAbilities}>{PokemonMoves}</Text>
    </ScrollView>

    </View>
    )
}

const styles = StyleSheet.create({
    fiches: {
        alignItems: "center",
        justifyContent: "center",
        margin: "auto",
    },
    image: {
        width: 100,
        height: 100,
        backgroundColor: "white",
        borderRadius: 160,
        textAlign: "center",
    },
    PokemonName: {
        fontSize: 18,
        textTransform: "uppercase",
        paddingTop: 10,
        paddingBottom: 20,
    }

});